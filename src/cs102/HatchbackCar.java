package cs102;

public class HatchbackCar extends Car {
    public HatchbackCar(double positionX, double positionY) {
        super(positionX, positionY);
        this.setColor("Black");
        this.setNumberOfDoors(5);
    }
}
