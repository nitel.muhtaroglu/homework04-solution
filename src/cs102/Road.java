package cs102;

import java.util.ArrayList;

public class Road {
    public String name;
    public ArrayList<Vehicle> vehicles;

    public Road(String name) {
        this.name = name;
        this.vehicles = new ArrayList<Vehicle>();
    }

    public void add(Vehicle vehicle) {
        this.vehicles.add(vehicle);
    }

    public void moveSportsCarBy(double displacementX, double displacementY) {
        for (Vehicle vehicle : vehicles) {
            if (vehicle instanceof SportsCar) {
                vehicle.moveBy(displacementX, displacementY);
            }
        }
    }

    public String toString() {
        String message = "";
        for (Vehicle vehicle : vehicles) {
            message += vehicle.toString();
        }
        return message;
    }

    public ArrayList<Vehicle> getVehicles() {
        return this.vehicles;
    }
}
