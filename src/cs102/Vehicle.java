package cs102;

public abstract class Vehicle {
    private double positionX;
    private double positionY;
    private String color;
    private int numberOfDoors;
    private int numberOfTires;

    public Vehicle(double positionX, double positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
        this.color = "white";
    }

    public double getPositionX() {
        return this.positionX;
    }

    public void setPositionX(double positionX) {
        this.positionX = positionX;
    }

    public double getPositionY() {
        return this.positionY;
    }

    public void setPositionY(double positionY) {
        this.positionY = positionY;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getNumberOfDoors() {
        return this.numberOfDoors;
    }

    public void setNumberOfDoors(int NumberOfDoors) {
        this.numberOfDoors = NumberOfDoors;
    }

    public int getNumberOfTires() {
        return this.numberOfTires;
    }

    public void setNumberOfTires(int NumberofTires) {
        this.numberOfTires = NumberofTires;
    }

    public String toString() {
        return "PositionX=" + this.getPositionX() + ", PositionY=" + this.getPositionY() + "\n";
    }

    public void moveBy(double displacementX, double displacementY) {
        this.positionX += displacementX;
        this.positionY += displacementY;
    }

}
