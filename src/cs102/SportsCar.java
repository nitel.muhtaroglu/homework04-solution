package cs102;

public class SportsCar extends Car {
    public SportsCar(double positionX, double positionY) {
        super(positionX, positionY);
        this.setColor("Red");
        this.setNumberOfDoors(2);
    }
}
