package cs102;

public class Main {

    public static void main(String[] args) {
        Road road = new Road("e5");
        /* Add three cars */
        road.add(new Car(0, 0));
        road.add(new Car(0, 10));
        road.add(new Car(0, 20));

        /* Add two SportsCars */
        road.add(new SportsCar(5, 0));
        road.add(new SportsCar(5, 10));

        /* Add five HatchbackCars */
        road.add(new HatchbackCar(20, 0));
        road.add(new HatchbackCar(20, 10));
        road.add(new HatchbackCar(20, 10));
        road.add(new HatchbackCar(20, 10));
        road.add(new HatchbackCar(20, 10));

        System.out.println(road);
        road.moveSportsCarBy(10, 0);
        System.out.println(road);
    }
}
