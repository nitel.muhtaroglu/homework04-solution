package cs102;

public class Car extends Vehicle {
    public Car(double positionX, double positionY) {
        super(positionX, positionY);
        this.setNumberOfTires(4);
        this.setNumberOfDoors(4);
    }
}
